/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableModel;

import database.Staff;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Kotori
 */
public class StaffTableModel extends AbstractTableModel {
    ArrayList<Staff> staffList = new ArrayList<Staff>();
    String[] columnNames = {"ลำดับ", "รหัสพนักงาน", "ชื่อ", "นามสกุล", "เบอร์โทรศัพท์", "เลขบัตรประชาชน", "รหัสผ่าน", "สถานะ"};
    @Override
    public int getRowCount() {
        return staffList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Staff staff = staffList.get(rowIndex);
        switch(columnIndex) {
            case 0 : return rowIndex+1 ;
            case 1: return staff.getStaffId();
            case 2: return staff.getFirstname();
            case 3: return staff.getSurname();
            case 4: return staff.getPhone();
            case 5: return staff.getIdcard();
            case 6: return staff.getPassword();
            case 7: return staff.getStatus()==0?"ทำงาน":"ลาออก";
        }
        return "";
    }   
    @Override
    public String getColumnName(int column){
        return columnNames[column];
    }
    
    public void setData(ArrayList<Staff> staffList) {
        this.staffList = staffList;
        fireTableDataChanged();
    }
}
