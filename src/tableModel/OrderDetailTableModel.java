package tableModel;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import database.DetailOrder;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author hafish
 */

public class OrderDetailTableModel extends AbstractTableModel{
    ArrayList<DetailOrder> orderDetailList = new ArrayList<DetailOrder>();
    String[] columnNames = {"รหัสสินค้า","ชื่อ","รุ่น","จำนวน(ใบ)","ราคา(บาท)","ราคารวม(บาท)"};
    
    
    @Override
    public int getRowCount() {
        return orderDetailList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        DetailOrder orderDetail = orderDetailList.get(rowIndex);
        switch(columnIndex){
            case 0 : return orderDetail.getProduct().getProductId() ;
            case 1 : return orderDetail.getProduct().getName() ;
            case 2 : return orderDetail.getProduct().getModel() ;
            case 3 : return orderDetail.getAmount() ; 
            case 4 : return orderDetail.getProduct().getPrice() ; 
            case 5 : return orderDetail.getTotal();
        }
        return "";
    }
    public void setData(ArrayList<DetailOrder> orderDetailList){
        this.orderDetailList = orderDetailList ;
        fireTableDataChanged();
    }
    
    @Override
    public String getColumnName(int column){
        return columnNames[column];
    }
}
