/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableModel;

import database.Product;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author User
 */
public class ProductTableModel extends AbstractTableModel{
    ArrayList<Product> productList = new ArrayList<Product>();
    String[] columnNames = {"รหัสสินค้า", "ชื่อสินค้า", "รุ่น", "ราคา", "จำนวนสินค้า"};
    
    public int getRowCount() {
        return productList.size();
    }

    
    public int getColumnCount() {
        return columnNames.length;
    }

    
    public Object getValueAt(int rowIndex, int columnIndex) {
        Product product = productList.get(rowIndex);
        switch(columnIndex) {
            case 0: return product.getProductId();
            case 1: return product.getName();
            case 2: return product.getModel();
            case 3: return product.getPrice();
            case 4: return product.getAmount();
            
        }
        return "";
    }   
    
    public String getColumnName(int column){
        return columnNames[column];
    }
    
    public void setData(ArrayList<Product> productList) {
        this.productList = productList;
        fireTableDataChanged();
    }
}
