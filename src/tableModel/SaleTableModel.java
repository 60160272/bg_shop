/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableModel;

import database.Sale;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ASUS
 */
public class SaleTableModel extends AbstractTableModel{
ArrayList<Sale> saleList = new ArrayList<Sale>();
    String[] columnNames = {"ลำดับ", "วันที่", "จำนวนรายการ", "จำนวนราคา"};

    
        public int getRowCount() {
        return saleList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Sale sale = saleList.get(rowIndex);
        switch(columnIndex) {
            case 0: return (rowIndex+1);
            case 1: return sale.getDate();
            case 2: return sale.getAmount();
            case 3: return sale.getPrice();
        }
        return "";
    }
    @Override
    public String getColumnName(int column){
        return columnNames[column];
    }
    
    public void setData(ArrayList<Sale> staffList) {
        this.saleList = staffList;
        fireTableDataChanged();
    }
    
}
