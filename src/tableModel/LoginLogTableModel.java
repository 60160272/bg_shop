/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableModel;

import database.LoginLog;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author mongkhon
 */
public class LoginLogTableModel extends AbstractTableModel {
    ArrayList<LoginLog> LoginLogList = new ArrayList<LoginLog>();
    String[] columnNames = {"ลำดับ", "รหัสพนักงาน", "วันที่", "เวลาเข้า","เวลาออก"};
    @Override
    public int getRowCount() {
        return LoginLogList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        LoginLog loginlog = LoginLogList.get(rowIndex);
        switch(columnIndex) {
            case 0: return rowIndex+1;
            case 1: return loginlog.getstaffId();
            case 2: return loginlog.getdate();
            case 3: return loginlog.gettimeIn();
            case 4: return loginlog.gettimeOut();
            
        }
        return "";
    }   
    @Override
    public String getColumnName(int column){
        return columnNames[column];
    }
    
    public void setData(ArrayList<LoginLog> LoginLogList) {
        this.LoginLogList = LoginLogList;
        fireTableDataChanged();
    }
}
