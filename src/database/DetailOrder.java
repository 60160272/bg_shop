/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author hafish
 */
public class DetailOrder {
    private int orderDetailId ;
    private int orderId ;

    private Product product ;
    private int amount ;
    private double total ;

    public DetailOrder(int orderDetailId,int orderId,Product product,int amount ,double total){
        this.orderDetailId = orderDetailId;
        this.orderId = orderId ;
        this.product =  product;
        this.amount = amount;
        this.total = amount*product.getPrice();
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    

    public int getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(int orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    public void calDetailTotal(){
        this.total = amount*product.getPrice();
    }

}