/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mongkhon
 */
public class MemberDao {

    public static boolean insert(Member member) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Member (\n"
                    + "                      firstname,\n"
                    + "                      surname,\n"
                    + "                      phone\n"
                    + "                  )\n"
                    + "                  VALUES (\n"
                    + "                      '%s',\n"
                    + "                      '%s',\n"
                    + "                      '%s'\n"
                    + "                  );";
            stm.execute(String.format(sql, 
                    member.getFirstname(), member.getSurname(), member.getPhone()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
//เพื่ม
    public static boolean update(Member member) {
        String sql = "UPDATE Member SET \n"
                + "       firstname = '%s',\n"
                + "       surname = '%s',\n"
                + "       phone = '%s'\n"
                + " WHERE memberId = %d;";
        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,
                    member.getFirstname(),
                    member.getSurname(),
                    member.getPhone(),
                    member.getMemberId()
            ));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    private static Member toObject(ResultSet rs) throws SQLException {
        Member member;
        member = new Member(
                rs.getInt("memberId"),
                rs.getString("firstname"),
                rs.getString("surname"),
                rs.getString("phone")
        );
        return member;
    }

    public static ArrayList<Member> getMembers() {
        ArrayList<Member> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT memberId,\n"
                    + "       firstname,\n"
                    + "       surname,\n"
                    + "       phone\n"
                    + "  FROM Member";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("memberId") + " " + rs.getString("firstname"));
                Member member = toObject(rs);
                list.add(member);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    
    public static Member getMember(int memberId) {
        String sql = "SELECT * FROM Member WHERE memberId = %d";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, memberId));
            if (rs.next()) {
                Member member = toObject(rs);
                Database.close();
                return member;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    
   //บันทึก
    public static void save(Member member) {
        if(member.getMemberId()<0) {
            insert(member);
        }else {
            update(member);
        }
    }
    
    public static Member phoneIsHave(String phone) {
        String sql = "SELECT * FROM Member WHERE phone = '%s'";
        Connection conn = Database.connect();
        System.out.println(String.format(sql, phone));
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, phone));
            if (rs.next()) {
                Member member = toObject(rs);
                Database.close();
                return member;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    
    

}