/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kotori
 */
public class StaffDao {

    public static boolean insert(Staff staff) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Staff (\n"
                    + "                      staffId,\n"
                    + "                      firstname,\n"
                    + "                      surname,\n"
                    + "                      phone,\n"
                    + "                      idcard,\n"
                    + "                      password,\n"
                    + "                      status,\n"
                    + "                      type\n"
                    + "                  )\n"
                    + "                  VALUES (\n"
                    + "                      null,\n"
                    + "                      '%s',\n"
                    + "                      '%s',\n"
                    + "                      '%s',\n"
                    + "                      '%s',\n"
                    + "                      '%s',\n"
                    + "                      %d,\n"
                    + "                      %d\n"
                    + "                  );";
            stm.execute(String.format(sql,  
                    staff.getFirstname(),
                    staff.getSurname(), 
                    staff.getPhone(), 
                    staff.getIdcard(),
                    staff.getPassword(), 
                    staff.getStatus(), 
                    staff.getType()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public static boolean update(Staff staff) {
        String sql = "UPDATE Staff SET \n"
                + "       firstname = '%s',\n"
                + "       surname = '%s',\n"
                + "       phone = '%s',\n"
                + "       idcard = '%s',\n"
                + "       password = '%s',\n"
                + "       status = %d,\n"
                + "       type = %d\n"
                + " WHERE staffId = %d;";
        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,
                    staff.getFirstname(),
                    staff.getSurname(),
                    staff.getPhone(),
                    staff.getIdcard(),
                    staff.getPassword(),
                    staff.getStatus(),
                    staff.getType(),
                    staff.getStaffId()
            ));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    public static boolean delete(Staff staff) {
        String sql = "DELETE FROM Staff WHERE staffId = %d;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, staff.getStaffId()));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    public static ArrayList<Staff> getStaffs() {
        ArrayList<Staff> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT staffId,\n"
                    + "       firstname,\n"
                    + "       surname,\n"
                    + "       phone,\n"
                    + "       idcard,\n"
                    + "       password,\n"
                    + "       status,\n"
                    + "       type\n"
                    + "  FROM Staff";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("staffId") + " " + rs.getString("firstname"));
                Staff staff = toObject(rs);
                list.add(staff);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Staff toObject(ResultSet rs) throws SQLException {
        Staff staff;
        staff = new Staff(
                rs.getInt("staffId"),
                rs.getString("firstname"),
                rs.getString("surname"),
                rs.getString("phone"),
                rs.getString("idcard"),
                rs.getString("password"),
                rs.getInt("status"),
                rs.getInt("type")
        );
        return staff;
    }

    public static Staff getStaff(int staffId) {
        String sql = "SELECT * FROM staff WHERE staffId = %d";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, staffId));
            if (rs.next()) {
                Staff staff = toObject(rs);
                Database.close();
                return staff;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    
    public static Staff checkLogin(int staffId, String password) {
        String sql = "SELECT * FROM Staff WHERE staffId = '%d' and  password = '%s' and status = 0";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, staffId,password));
            if (rs.next()) {
                Staff staff = toObject(rs);
                Database.close();
                return staff;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    public static void save(Staff staff) {
        if (staff.getStaffId() < 0) {
            insert(staff);
        } else {
            update(staff);
        }
    }
}
