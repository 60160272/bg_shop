    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */
    package database;

    /**
     *
     * @author hafish
     */
    public class Order {
        private int orderId ;
        private int staffId ;
        private int memberId ;
        private int amount ;
        private double price ;
        private double discount ;
        private double total;
        private String dateTime ;
        private double getTotal ;
        private double giveTotal ;

        public Order(){

        }

        public Order(int orderId, int staffId, int memberId, int amount, double price,double discount, double total, String dateTime, double getTotal, double giveTotal) {
            this.orderId = orderId;
            this.staffId = staffId;
            this.memberId = memberId;
            this.amount = amount;
            this.price = price ;
            this.discount = discount;
            this.total = total;
            this.dateTime = dateTime;
            this.getTotal = getTotal;
            this.giveTotal = giveTotal;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public Order(int staffId, int memberId, int amount, double price,double discount, double total) {
            this.orderId = -1;
            this.staffId = staffId;
            this.memberId = memberId;
            this.amount = amount;
            this.price = price ;
            this.discount = discount;
            this.total = total;
            this.dateTime = null;
            this.getTotal = -1;
            this.giveTotal = -1;
        }

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
    }

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public double getGetTotal() {
        return getTotal;
    }

    public void setGetTotal(double getTotal) {
        this.getTotal = getTotal;
    }

    public double getGiveTotal() {
        return giveTotal;
    }

    public void setGiveTotal(double giveTotal) {
        this.giveTotal = giveTotal;
    }
}
