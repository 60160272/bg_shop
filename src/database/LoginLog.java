/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.ResultSet;

/**
 *
 * @author informatics
 */
public class LoginLog {

    public static ResultSet getData(String select__from_LoginLog_WHERE_loginId__d) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    int loginId;
    int staffId;
    String date;
    String timeIn;
    String timeOut;
    
   

    public LoginLog(int loginId,int staffId, String date, String timeIn, String timeOut) {
        this.loginId = loginId;
        this.staffId = staffId;
        this.date = date;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
    }
        public LoginLog(int loginId,String timeOut) {
        this.loginId = loginId;
        this.timeOut = timeOut;
    }
    

    LoginLog(int aInt, int aInt0, String string, String string0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

    
       
    public int getloginId() {
        return loginId;
    }

    public void setloginId(int loginId) {
        this.loginId = loginId;
    }

    public int getstaffId() {
        return staffId;
    }

    public void setstaffId(int staffId) {
        this.staffId = staffId;
    }

    public String getdate() {
        return date;
    }

    public void setdate(String date) {
        this.date = date;
    }

    public String gettimeIn() {
        return timeIn;
    }

    public void settimeIn(String timeIn) {
        this.timeIn = timeIn;
    }
    
    public String gettimeOut() {
        return timeOut;
    }

    public void settimeOut(String timeOut) {
        this.timeOut = timeOut;
        
    }
   

    
     @Override
    public String toString() {
        return "LoginLog{" + "loginId=" + loginId + ", staffId=" + staffId + ", date=" + date + ", timeIn=" + timeIn + ", timeOut=" + timeOut + '}';
    }

    
    

}
