/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author hafish
 */
public class Database {
        static String url = "jdbc:sqlite:./db/bg_shop.db";
    static Connection conn = null;  
    
    public static Connection connect() {
        if(conn!=null) return conn;
        try {
            conn = DriverManager.getConnection(url);
            return conn;
        } catch (SQLException ex) {
            System.out.println("Connect Database not success");
        }
        return null;
    }
    public static void close() {
        try {
            if(conn!=null) {
                conn.close();
                conn = null;
            }
        } catch (SQLException ex) {
            System.out.println("Close Database not success");
        }

    }
}
