/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author werap
 */
public class OrderDao {
    public static boolean insert(Order order) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO [Order] (\n"
                    + "                     staffId,\n"
                    + "                     memberId,\n"
                    + "                     amount,\n"
                    + "                     price,\n"
                    + "                     discount,\n"
                    + "                     total)\n"
                    + "                 VALUES (\n"
                    + "                     '%d',\n"
                    + "                     '%d',\n"
                    + "                     '%d',\n"
                    + "                     '%f',\n"
                    + "                     '%f',\n"
                    + "                     '%f');";

            stm.execute(String.format(sql,
                    order.getStaffId(),
                    order.getMemberId(),
                    order.getAmount(),
                    order.getPrice(),
                    order.getDiscount(),
                    order.getTotal()
            ));
            Database.close();
            return true;
        } catch (SQLException ex) {

        }
        Database.close();
        return true;
    }
    public static boolean update(Order order) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "UPDATE [Order]\n" +
                        "   SET staffId = '%d',\n" +
                        "       memberId = '%d',\n" +
                        "       amount = '%d',\n" +
                        "       price = '%f',\n" +
                        "       discount = '%f',\n" +
                        "       total = '%f',\n" +
                        "       getTotal = '%f',\n" +
                        "       giveTotal = '%f'\n" +
                        "       WHERE orderId = (select orderId from [Order] order by orderId DESC limit 1) ";

            String sqlFormat = String.format(sql,
                    order.getStaffId(),
                    order.getMemberId(),
                    order.getAmount(),
                    order.getPrice(),
                    order.getDiscount(),
                    order.getTotal(),
                    order.getGetTotal(),
                    order.getGiveTotal());

            stm.executeQuery(sqlFormat);
            Database.close();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }
    

    public static boolean delete(Order order) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "DELETE FROM [Order]\n"
                    + "WHERE orderId = %d";
            System.out.println(String.format(sql,order.getOrderId()));
            String sqlFormat = String.format(sql,
                    order.getOrderId());

            stm.executeQuery(sqlFormat);
            Database.close();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public static ArrayList<Order> getOrders() {
        ArrayList<Order> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * "
                    + "  FROM order";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Order order = toObject(rs);
                list.add(order);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }

    public static Order getOrder(int userId) {
        Database.conn = Database.connect();
        try {
            Statement stm = Database.conn.createStatement();
            String sql = "SELECT * FROM user WHERE userId = %d";
            ResultSet rs = stm.executeQuery(String.format(sql, userId));
            while (rs.next()) {
                Order order = toObject(rs);
                Database.close();
                return order;
            }
        } catch (SQLException e) {

        }
        return null;
    }
    
    public static Order getLastOrder() {
        Database.conn = Database.connect();
        try {
            Statement stm = Database.conn.createStatement();
            String sql = "SELECT * FROM [Order] order by orderId DESC limit 1";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Order order = toObject(rs);
                Database.close();
                return order;
            }
        }
        catch (SQLException e) {

        }
        return null;
    }
    
        public static ArrayList<Sale> showSaleDay() {
        ArrayList<Sale> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "select \n" +
                        "strftime('%d-%m-%Y', dateTime) as date,\n" +
                        "sum(amount) as amount,\n" +
                        "sum(Total) as price\n" +
                        "from [order]\n where getTotal != -1 \n" +
                        "group by date\n" +
                        "order by dateTime DESC";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Sale sale = toObjectSale(rs);
                list.add(sale);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }
    
    public static Sale showSaleThisDay() {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "select \n" +
                        "strftime('%d-%m-%Y', dateTime) as date,\n" +
                        "sum(amount) as amount,\n" +
                        "sum(Total) as price\n" +
                        "from [order]\n where getTotal != -1 \n and"
                    + " date = "+"strftime('%d-%m-%Y',	\n" +"CURRENT_TIMESTAMP )"+"" +
                        "group by date\n" +
                        "order by dateTime DESC";
            System.out.println(sql);
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Sale sale = toObjectSale(rs);
                return sale;
            }
            Database.close();
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }

    public static ArrayList<Sale> showSaleMonth() {
        ArrayList<Sale> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "select \n" +
                        "strftime('%m-%Y', dateTime) as date,\n" +
                        "sum(amount) as amount,\n" +
                        "sum(Total) as price\n" +
                        "from [order]\n where getTotal != -1 \n " +
                        "group by date\n" +
                        "order by dateTime DESC";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Sale sale = toObjectSale(rs);
                list.add(sale);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }

    public static ArrayList<Sale> showSaleYears() {
        ArrayList<Sale> list = new ArrayList(); 
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "select \n" +
                        "strftime('%Y', dateTime) date,\n" +
                        "sum(amount) as amount,\n" +
                        "sum(Total) as price\n" +
                        "from [order] \n where getTotal != -1 \n" +
                        "group by date\n" +
                        "order by dateTime DESC";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Sale sale = toObjectSale(rs);
                list.add(sale);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }

    public static ArrayList<Sale> showSale() {
        ArrayList<Sale> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "select \n" +
                        "strftime('%d-%m-%Y %H:%M:%S', dateTime) as date,\n" +
                        "amount ,\n" +
                        "Total as price\n" +
                        "from [order]\n where getTotal != -1 \n" +
                        "order by dateTime DESC";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Sale sale = toObjectSale(rs);
                list.add(sale);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }

    public static double sumTotal() {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "select sum(total) as total from [order]";
            ResultSet rs = stm.executeQuery(sql);
            double total = rs.getDouble("total") ;
            Database.close();
            return total;
        } catch (SQLException ex) {

        }
        Database.close();
        return -1;
    }

    private static Order toObject(ResultSet rs) throws SQLException {
        Order order;
        order = new Order(
                rs.getInt("orderId"),
                rs.getInt("staffId"),
                rs.getInt("memberId"),
                rs.getInt("amount"),
                rs.getDouble("price"),
                rs.getDouble("discount"),
                rs.getDouble("total"),
                rs.getString("datetime"),
                rs.getDouble("getTotal"),
                rs.getDouble("giveTotal")
        );
        return order;
    }

    private static Sale toObjectSale(ResultSet rs) throws SQLException {
        Sale sale;
        sale = new Sale(
                rs.getString("date"),
                rs.getInt("amount"),
                rs.getInt("price")
        );
        return sale;
    }

}
