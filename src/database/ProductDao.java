/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class ProductDao {

    public static boolean insert(Product product) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Product (\n"
                    + "                        name,\n"
                    + "                        model,\n"
                    + "                        price,\n"
                    + "                        amount\n"
                    + "                    )\n"
                    + "                    VALUES (\n"
                    + "                        '%s',\n"
                    + "                        '%s',\n"
                    + "                        '%f',\n"
                    + "                        '%d'\n"
                    + "                    );";
            System.out.println(sql);
            stm.execute(String.format(sql,  product.getName(),
                    product.getModel(), product.getPrice(), product.getAmount()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public static boolean update(Product product) {
        String sql = "UPDATE Product SET \n"
                + "       name = '%s',\n"
                + "       model = '%s',\n"
                + "       price = '%f',\n"
                + "       amount = '%d'\n"
                + " WHERE productId = '%d'";
        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,
                    product.getName(),
                    product.getModel(),
                    product.getPrice(),
                    product.getAmount(),
                    product.getProductId()
            ));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    public static boolean delete(Product product) {
        String sql = "DELETE FROM Product WHERE productId = %d;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, product.getProductId()));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();        return false;
    }

    public static ArrayList<Product> getProducts() {
        ArrayList<Product> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT productId,\n"
                    + "       name,\n"
                    + "       model,\n"
                    + "       price,\n"
                    + "       amount\n"
                    + "  FROM Product";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("productId") + " " + rs.getString("name"));
                Product product = toObject(rs);
                list.add(product);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Product toObject(ResultSet rs) throws SQLException {
        Product product;
        product = new Product(
                rs.getInt("productId"),
                rs.getString("name"),
                rs.getString("model"),
                rs.getDouble("price"),
                rs.getInt("amount")
        );
        return product;
    }

    public static Product getProduct(int productId) {
        String sql = "SELECT * FROM product WHERE productId = %d";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, productId));
            if (rs.next()) {
                Product product = toObject(rs);
                Database.close();
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    public static void save(Product product) {
        if (product.getProductId() < 0) {
            insert(product);
        } else {
            update(product);
        }
    }
}
