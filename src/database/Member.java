/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author mongkhon
 */
public class Member {

    int memberId;
    String firstname;
    String surname;
    String phone;
    String name;

    public Member(int memberId, String firstname, String surname, String phone) {
        this.memberId = memberId;
        this.firstname = firstname;
        this.surname = surname;
        this.phone = phone;
    }

    /**/ public Member() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return firstname + " " + surname;
    }

    public void setName() {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Member{" + "memberId=" + memberId + ", firstname=" + firstname + ", surname=" + surname + ", phone=" + phone + '}';
    }

}
