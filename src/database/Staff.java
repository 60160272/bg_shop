/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Kotori
 */
public class Staff {

    int staffId;
    String firstname;
    String surname;
    String phone;
    String idcard;
    String password;
    int status;
    int type;

    public Staff(int staffId, String firstname, String surname, String phone, String idcard, String password, int status, int type) {
        this.staffId = staffId;
        this.firstname = firstname;
        this.surname = surname;
        this.phone = phone;
        this.idcard = idcard;
        this.password = password;
        this.status = status;
        this.type = type;
    }

    public Staff() {
    
    }

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Staff{" + "staffId=" + staffId + ", firstname=" + firstname + ", surname=" + surname + ", phone=" + phone + ", idcard=" + idcard + ", password=" + password + ", status=" + status + ", type=" + type + '}';
    }

}