/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class LoginLogDao {

    public static ArrayList<LoginLog> ShowLoginLog;
    private static Object sumDay;

    /**
     *
     *
     * @return
     */
  

    private static LoginLog toObject(ResultSet rs) throws SQLException {
        LoginLog loginlog;
        loginlog = new LoginLog(
                rs.getInt("loginId"),
                rs.getInt("staffId"),
                rs.getString("date"),
                rs.getString("timeIn"),
                rs.getString("timeOut")
        );
        return loginlog;
    }
    
    public static boolean insert(int staffId) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Loginlog (staffId)\n"
                    + "                  VALUES ('%d');";
            System.out.println(String.format(sql, staffId));
            stm.execute(String.format(sql, staffId));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(LoginLogDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
//เพื่ม

    public static boolean update(LoginLog loginlog) {
        String sql = "UPDATE LoginLog SET \n"
                + "       timeOut = strftime('%H:%M:%S', CURRENT_TIMESTAMP)\n"
                + " WHERE loginId = "+loginlog.getloginId()+";";
        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(sql);
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(LoginLogDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return false;
    }

    

    public static ArrayList<LoginLog> getLoginLog() {
        ArrayList<LoginLog> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT loginId,\n"
                    + "       staffId,\n"
                    + "       date,\n"
                    + "       timeIn,\n"
                    + "       timeOut"
                    + "  FROM LoginLog order by date DESC";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("loginId") + " " + rs.getInt("staffId"));
                LoginLog loginlog = toObject(rs);
                list.add(loginlog);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(LoginLogDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    
    public static LoginLog getLastLoginLog() {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT loginId,\n"
                    + "       staffId,\n"
                    + "       date,\n"
                    + "       timeIn,\n"
                    + "       timeOut"
                    + "  FROM LoginLog order by loginId DESC limit 1";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("loginId") + " " + rs.getInt("staffId"));
                LoginLog loginlog = toObject(rs);
                return loginlog;
            }
            Database.close();
        } catch (SQLException ex) {
            Logger.getLogger(LoginLogDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
 

    public static ArrayList<LoginLog> searchButton(int staffId) {
        ArrayList<LoginLog> list = new ArrayList();
        String sql = "SELECT * FROM LoginLog     "
                + "WHERE staffId = '%d' "
                + "AND strftime('%%m/%%Y', date) = strftime('%%m/%%Y', CURRENT_TIMESTAMP) group by strftime('%%m/%%d', date)"
                + "order by date DESC";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            System.out.println(String.format(sql, staffId));
            ResultSet rs = stm.executeQuery(String.format(sql, staffId));
            while (rs.next()) {
                LoginLog loginlog = toObject(rs);
                list.add(loginlog);

            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(LoginLogDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }


}
