/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author hafish
 */
public class DetailOrderDao {

    public static boolean insert(DetailOrder detailOrder) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO [DetailOrder] (\n"
                    + "                     orderId,\n"
                    + "                     productId,\n"
                    + "                     amount,\n"
                    + "                     total)\n"
                    + "                 VALUES (\n"
                    + "                     (select orderId from [Order] order by orderId DESC limit 1),\n"
                    + "                     '%d',\n"
                    + "                     '%d',\n"
                    + "                     '%f');";

            stm.execute(String.format(sql,
                    detailOrder.getProduct().getProductId(),
                    detailOrder.getAmount(),
                    detailOrder.getTotal()
            ));
            Database.close();
            return true;
        } catch (SQLException ex) {

        }
        Database.close();
        return true;
    }

    public static boolean update(DetailOrder detailOrder) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "UPDATE DetailOrder set\n"
                    + "       amount = '%d',\n"
                    + "       total = '%f' \n"
                    + "where orderId = (select orderId from [Order] \n"
                    + "order by orderId DESC limit 1) \n"
                    + "and productId = '%d' ;";
//            System.out.println(String.format(sql,
//                    detailOrder.getAmount(),
//                    detailOrder.getTotal(),
//                    detailOrder.getProduct().getProductId()));
            boolean rs = stm.execute(String.format(sql,
                    detailOrder.getAmount(),
                    detailOrder.getTotal(),
                    detailOrder.getProduct().getProductId()
            ));
            Database.close();
            return rs;

        } catch (SQLException ex) {
            return false;
        }
    }

    public static ArrayList<DetailOrder> getOrderDetails() {
        ArrayList<DetailOrder> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT * "
                    + "  FROM DetailOrder";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                DetailOrder orderDetail = toObject(rs);
                list.add(orderDetail);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }

    public static ArrayList<DetailOrder> getOrderDetails(int orderId) {
        ArrayList<DetailOrder> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT *,DetailOrder.amount as amountOrder ,Product.amount as amountProduct\n"
                    + "FROM [DetailOrder],Product where orderId = '" + orderId + "' and DetailOrder.productId = Product.productId";

            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                DetailOrder orderDetail = toObject(rs);
                list.add(orderDetail);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }

    public static DetailOrder productIsHave(int productId) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT *,DetailOrder.amount as amountOrder ,Product.amount as amountProduct\n"
                    + "FROM [DetailOrder],Product "
                    + "where orderId = (select orderId from [Order] "
                    + "order by orderId DESC limit 1) "
                    + "and DetailOrder.productId = Product.productId "
                    + "and DetailOrder.productId = '" + productId + "' ;";

            ResultSet rs = stm.executeQuery(sql);
            if (rs.next()) {
                DetailOrder orderDetail = toObject(rs);
                Database.close();
                return orderDetail;
            }
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }

    public static ArrayList<DetailOrder> getLastOrderDetails() {
        ArrayList<DetailOrder> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT *,DetailOrder.amount as amountOrder ,Product.amount as amountProduct\n"
                    + "FROM [DetailOrder],Product where orderId = \n"
                    + "(select orderId from [Order] order by  orderId DESC limit 1)  and DetailOrder.productId = Product.productId";

            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                DetailOrder orderDetail = toObject(rs);
                list.add(orderDetail);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {

        }
        Database.close();
        return null;
    }

    private static DetailOrder toObject(ResultSet rs) throws SQLException {
        DetailOrder orderDetail = null;
        Product product = new Product(
                rs.getInt("productId"), rs.getString("name"), rs.getString("model"), rs.getDouble("price"), rs.getInt("amountProduct"));

        orderDetail = new DetailOrder(
                rs.getInt("orderDetailId"),
                rs.getInt("orderId"),
                product,
                rs.getInt("amountOrder"),
                rs.getDouble("total")
        );
        return orderDetail;
    }

}
