package UI;

import static UI.MainFrame.iconCorrect;
import database.Member;
import database.MemberDao;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import tableModel.MemberTableModel;

public class MemberPanel extends javax.swing.JPanel {

    public MemberPanel() {
        initComponents();
        updateTable();
        setStatus(FormState.INIT);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        nameText = new javax.swing.JTextField();
        phoneText = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        surnameText = new javax.swing.JTextField();
        memberIdText = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        addButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        clearButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        memberTable = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel4.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        jLabel4.setText("ชื่อ");

        jLabel2.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        jLabel2.setText("เบอร์โทร");

        nameText.setFont(new java.awt.Font("Angsana New", 0, 24)); // NOI18N
        nameText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameTextActionPerformed(evt);
            }
        });

        phoneText.setFont(new java.awt.Font("Angsana New", 0, 24)); // NOI18N
        phoneText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                phoneTextActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        jLabel3.setText("นามสกุล");

        surnameText.setFont(new java.awt.Font("Angsana New", 0, 24)); // NOI18N
        surnameText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                surnameTextActionPerformed(evt);
            }
        });

        memberIdText.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N

        jLabel6.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        jLabel6.setText("รหัสลูกค้า");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(memberIdText, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(phoneText, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 79, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nameText, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(surnameText, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(memberIdText, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nameText, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(surnameText, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(phoneText, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 121, -1, -1));

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        addButton.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        addButton.setText("เพิ่ม");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        saveButton.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        saveButton.setText("บันทึก");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        clearButton.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        clearButton.setText("ล้างข้อมูล");
        clearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addButton, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(clearButton, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(clearButton, javax.swing.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE)
                    .addComponent(saveButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(addButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 257, 726, -1));

        memberTable.setModel(memberTableModel);
        memberTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                memberTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(memberTable);
        if (memberTable.getColumnModel().getColumnCount() > 0) {
            memberTable.getColumnModel().getColumn(0).setResizable(false);
        }

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 368, 726, 303));

        jLabel5.setFont(new java.awt.Font("TH Sarabun New", 1, 48)); // NOI18N
        jLabel5.setText("จัดการลูกค้าสมาชิก");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 30, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void nameTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameTextActionPerformed

    private void surnameTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_surnameTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_surnameTextActionPerformed

    private void phoneTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_phoneTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_phoneTextActionPerformed

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        if (checkName() && checkSurname() && checkPhone()) {
            setStatus(FormState.ADDNEW);
            saveMember();
            MemberDao.insert(member);
            updateTable();
            clearForm();
            JOptionPane.showMessageDialog(null, "บันทึกข้อมูลเรียบร้อย", "Correct", JOptionPane.INFORMATION_MESSAGE, iconCorrect);
        }
    }//GEN-LAST:event_addButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        if (checkName() && checkSurname() && checkPhone()) {
            saveMember();
            MemberDao.save(member);
            updateTable();
            clearForm();
            setStatus(FormState.INIT);
            JOptionPane.showMessageDialog(null, "บันทึกข้อมูลเรียบร้อย", "Correct", JOptionPane.INFORMATION_MESSAGE, iconCorrect);
        }
    }

    private void updateTable() {
        memberTableModel.setData(MemberDao.getMembers());
    }//GEN-LAST:event_saveButtonActionPerformed

    private void clearButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearButtonActionPerformed
        clearForm();
        setStatus(FormState.INIT);
    }//GEN-LAST:event_clearButtonActionPerformed

    private void memberTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_memberTableMouseClicked
        setStatus(FormState.UPDATE);
        String idStr = memberTable.getValueAt(memberTable.getSelectedRow(), 0).toString();
        int id = Integer.parseInt(idStr);
        member = MemberDao.getMember(id);
        chooseData(member);
    }//GEN-LAST:event_memberTableMouseClicked

    private Member member;
    MemberTableModel memberTableModel = new MemberTableModel();
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JButton clearButton;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel memberIdText;
    private javax.swing.JTable memberTable;
    private javax.swing.JTextField nameText;
    private javax.swing.JTextField phoneText;
    private javax.swing.JButton saveButton;
    private javax.swing.JTextField surnameText;
    // End of variables declaration//GEN-END:variables

    private boolean checkName() {
        String name = nameText.getText();
        try {
            if (name.equals("")) {
                JOptionPane.showMessageDialog(null, "กรุณากรอกชื่อ", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            } else if (name.equals("!") || name.equals("@") || name.equals("#") || name.equals("$") || name.equals("%") || name.equals("^") || name.equals("&")
                    || name.equals("*") || name.equals("-") || name.equals("+") || name.equals("/") || name.equals("_") || name.equals("=") || name.equals("~")
                    || name.equals(".") || name.equals("|") || name.equals("ฺ") || name.equals(",") || name.equals("?") || name.equals(":") || name.equals(";")
                    || name.equals("๐") || name.equals("0") || name.equals("1") || name.equals("2") || name.equals("3") || name.equals("4") || name.equals("5")
                    || name.equals("6") || name.equals("7") || name.equals("8") || name.equals("9") || name.equals("\\")) {
                JOptionPane.showMessageDialog(null, "กรุณากรอกเบอร์โทร์ศัพท์ให้ครบ 10 หลัก", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "กรุณากรอกชื่อให้ถูกต้อง", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    private boolean checkSurname() {
        String surname = surnameText.getText();
        try {
            if (surname.equals("")) {
                JOptionPane.showMessageDialog(null, "กรุณากรอกนามสกุล", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            } else if (surname.equals("!") || surname.equals("@") || surname.equals("#") || surname.equals("$") || surname.equals("%") || surname.equals("^") || surname.equals("&")
                    || surname.equals("*") || surname.equals("-") || surname.equals("+") || surname.equals("/") || surname.equals("_") || surname.equals("=") || surname.equals("~")
                    || surname.equals(".") || surname.equals("|") || surname.equals("ฺ") || surname.equals(",") || surname.equals("?") || surname.equals(":") || surname.equals(";")
                    || surname.equals("๐") || surname.equals("0") || surname.equals("1") || surname.equals("2") || surname.equals("3") || surname.equals("4") || surname.equals("5")
                    || surname.equals("6") || surname.equals("7") || surname.equals("8") || surname.equals("9") || surname.equals("\\")) {
                JOptionPane.showMessageDialog(null, "กรุณากรอกเบอร์โทร์ศัพท์ให้ครบ 10 หลัก", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "กรุณากรอกนามสกุลให้ถูกต้อง", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    private boolean checkPhone() {
        String phone = phoneText.getText();
        try {
            if (phone.equals("")) {
                JOptionPane.showMessageDialog(null, "กรุณากรอกเบอร์โทร์ศัพท์", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            } else if (MemberDao.phoneIsHave(phone) != null) {
                JOptionPane.showMessageDialog(null, "เบอร์โทร์ศัพท์นี้ถูกใช้ไปแล้ว", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            } else if (phone.length() != 10) {
                JOptionPane.showMessageDialog(null, "กรุณากรอกเบอร์โทร์ศัพท์ให้ครบ 10 หลัก", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            } else if (phone.equals("!") || phone.equals("@") || phone.equals("#") || phone.equals("$") || phone.equals("%") || phone.equals("^") || phone.equals("&") || phone.equals("*")
                    || phone.equals("-") || phone.equals("+") || phone.equals("/") || phone.equals("_") || phone.equals("=") || phone.equals("~") || phone.equals(".") || phone.equals("|")
                    || phone.equals("ฺ") || phone.equals(",") || phone.equals("?") || phone.equals(":") || phone.equals(";") || phone.equals("๐") || phone.equals("\\")) {
                JOptionPane.showMessageDialog(null, "เบอร์โทร์ศัพท์ต้องเป็นตัวเลข", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            Long.parseLong(phone);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "เบอร์โทร์ศัพท์ต้องเป็นตัวเลข", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    private void chooseData(Member member) {
        memberIdText.setText("" + member.getMemberId());
        nameText.setText(member.getFirstname());
        surnameText.setText(member.getSurname());
        phoneText.setText(member.getPhone());
    }

    private void saveMember() {
        member = new Member();
        if (memberIdText.getText().length() != 0) {
            member.setMemberId(Integer.parseInt(memberIdText.getText()));
        }
        member.setFirstname(nameText.getText());
        member.setSurname(surnameText.getText());
        member.setPhone(phoneText.getText());
    }

    private void clearForm() {
        memberIdText.setText("");
        nameText.setText("");
        surnameText.setText("");
        phoneText.setText("");

    }

    private void setStatus(FormState state) {
        switch (state) {
            case INIT:
                addButton.setEnabled(true);
                saveButton.setEnabled(false);
                clearButton.setEnabled(false);
                break;
            case ADDNEW:
                addButton.setEnabled(false);
                saveButton.setEnabled(true);
                clearButton.setEnabled(true);
                break;
            case UPDATE:
                addButton.setEnabled(false);
                saveButton.setEnabled(true);
                clearButton.setEnabled(true);
                break;
        }
    }
}
